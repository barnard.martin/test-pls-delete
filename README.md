## Name
Test please delete

## Description
We are testing a full pipeline with CI/CD from gitlab into azure using terraform stored in gitlab
Info from here: https://docs.microsoft.com/en-us/shows/devops-lab/gitlab-into-azure

According to the Azure Pipeline documentation, they only support GitHub and Azure Repos:
```
Version control systems

The starting point for configuring CI and CD for your applications is to have your source code in a version control system. Azure DevOps supports two forms of version control - GitHub and Azure Repos. Any changes you push to your version control repository will be automatically built and validated.
```

However, this tutorial offers hope:
https://docs.microsoft.com/en-us/shows/devops-lab/gitlab-into-azure

Possible flow for success:
Create GitLab pipeline to trigger Azure pull/clone & build:
https://marketplace.visualstudio.com/items?itemName=onlyutkarsh.gitlab-integration



## Installation
* Install azure-cli
* Login to azure via cli
* Install terraform
* TODO: Dockerise the setup

## Usage
`az login`
`terraform init`
`terraform plan`


## Authors and acknowledgment
Martin Barnard <barnard.martin@gmail.com>
