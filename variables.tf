variable "name_prefix" {
  default     = "postgresqlfs"
  description = "Prefix of the resource name."
}

variable "location" {
  default     = "eastus"
  description = "Location of the resource."
}


variable "pg_admin_login" {
  default     = "adminTerraform"
  description = "Login name of admin user"
}

variable "pg_admin_pwd" {
  default     = "QAZwsx123"
  description = "Password for admin - we would have this stored in a secrets file"
}
